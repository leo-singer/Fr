#
# CMake packaging for librame
# Copyright 2019  Duncan Macleod <duncan.macleod@ligo.org>
#

cmake_minimum_required(VERSION 3.12.0 FATAL_ERROR)
project(
    framel
    LANGUAGES C
    VERSION 8.41.1
    DESCRIPTION "LIGO/VIRGO frame library"
    HOMEPAGE_URL "https://git.ligo.org/virgo/virgoapp/Fr"
)

include(GNUInstallDirs)
include(CheckFunctionExists)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -DG__ROOT -D_XOPEN_SOURCE=700")
#set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -DG__ROOT -D_ISOC99_SOURCE")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DFR_PATH=\\\"${CMAKE_CURRENT_SOURCE_DIR}\\\"")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DFR_VERSION=\\\"${PROJECT_VERSION}\\\"")
set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG}")
set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -g")

# -- set install paths ------

if (WIN32)
    set(RUNTIME_DESTINATION ${CMAKE_INSTALL_BINDIR})
    set(LIBRARY_DESTINATION ${CMAKE_INSTALL_BINDIR})
else (WIN32)
    set(RUNTIME_DESTINATION ${CMAKE_INSTALL_LIBDIR})
    set(LIBRARY_DESTINATION ${CMAKE_INSTALL_LIBDIR})
endif(WIN32)

# -- build components -------

# C library | enable/disable with -DENABLE_C={yes,no} (default yes)
add_subdirectory(src)
add_subdirectory(doc)

# Matlab library | enable/disable with -DENABLE_MATLAB={yes,no} (default no)
add_subdirectory(matlab)

# Python library | enable/disable with -DENABLE_PYTHON={yes,no} (default no)
add_subdirectory(Python)

# -- build tarball ----------
#
# to build a source tarball:
#
# mkdir dist
# pushd dist
# cmake ..
# cmake --build . --target package_source
#

set(CPACK_PACKAGE_MAJOR ${${PROJECT_NAME}_MAJOR_VERSION})
set(CPACK_PACKAGE_MINOR ${${PROJECT_NAME}_MINOR_VERSION})
set(CPACK_PACKAGE_PATCH ${${PROJECT_NAME}_PATCH_VERSION})
set(CPACK_PACKAGE_VERSION "${CPACK_PACKAGE_MAJOR}.${CPACK_PACKAGE_MINOR}.${CPACK_PACKAGE_PATCH}")

set(CPACK_SOURCE_GENERATOR TXZ)
set(CPACK_SOURCE_PACKAGE_FILE_NAME ${PROJECT_NAME}-${${PROJECT_NAME}_VERSION})
set(CPACK_SOURCE_IGNORE_FILES
    "/.*~$/"
    ".*~$"
    "\\\\.svn/"
    "\\\\.git"
    "build/"
    "CMakeFiles/"
    "CMakeCache.txt"
    "_CPack_Packages/"
    "\\\\.cmake"
    "Makefile"
    "\\\\.deps/"
    "autom4te.cache/"
    "\\\\.tar\\\\.xz"
)
include(CPack)
